package b137.alinabon.s03a1;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args){
        System.out.println("Factorial\n");

        // Activity
        // Create a Java program that accepts an integer and computes for
        // the factorial value and display it to the console.
        Scanner appScanner = new Scanner(System.in);
        System.out.println("Input a number : ");
        int number = appScanner.nextInt();
        int factor = 1;

        for(int i = 1; i <= number; i++){
            factor = factor * i;
        }

        System.out.println("The Factorial of " + number + " is " + factor);
    }
}
